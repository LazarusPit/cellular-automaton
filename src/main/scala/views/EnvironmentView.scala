package ch.ben_kelly
package views

import CellStates.CellState
import models.EnvironmentModel

import scalafx.collections.ObservableBuffer
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.control.{Button, Menu, MenuBar}
import scalafx.scene.layout._
import scalafx.scene.paint.Color
import scalafx.scene.shape.Rectangle

class EnvironmentView extends BorderPane {

  def apply(): EnvironmentView = new EnvironmentView()

  top = {
    val menuBar: MenuBar = new MenuBar()
    menuBar.alignmentInParent = Pos.Center
    BorderPane.setAlignment(menuBar, Pos.Center)
    menuBar.menus.add(new Menu("File"))
    menuBar.menus.add(new Menu("Edit"))
    menuBar.menus.add(new Menu("Help"))
    menuBar
  }

  val runButton: Button = new Button("Run")
  val grid: GridPane = new GridPane()
  val settingsButton: Button = new Button("Settings")

  center = {
    val container: VBox = new VBox()
    container.spacing = 20
    container.alignment = Pos.Center
    BorderPane.setAlignment(container, Pos.Center)

    grid.setGridLinesVisible(true)
    grid.maxWidth <== container.width
    grid.maxHeight <== grid.maxWidth
    grid.rowConstraints.clear()
    grid.columnConstraints.clear()
    VBox.setVgrow(grid, Priority.Always)

    container.children.add(settingsButton)
    container.children.add(grid)
    container.children.add(runButton)

    container.padding = Insets(30, 20, 30, 20)

    container
  }

  def bindToEnvironment(environmentModel: EnvironmentModel): Unit = {
    grid.columnConstraints.clear()
    grid.rowConstraints.clear()

    (0 until environmentModel.height).foreach(_ => {
      val rowConstraints = new RowConstraints(10, 10, Double.MaxValue)
      rowConstraints.percentHeight = 100 / environmentModel.height
      grid.rowConstraints.add(rowConstraints)
    })
    (0 until environmentModel.width).foreach(_ => {
      val colConstraints = new ColumnConstraints(10, 10, Double.MaxValue)
      colConstraints.percentWidth = 100 / environmentModel.width
      grid.columnConstraints.add(colConstraints)
    })

    (0 until environmentModel.height).foreach(rowIndex => {
      environmentModel.getCellsObservable(rowIndex).onChange((_, changes) => {
        for (change <- changes) {
          change match {
            case ObservableBuffer.Add(position, added) => added.zipWithIndex.foreach((addition, colDiff) => paintCell(colDiff + position, rowIndex, environmentModel.getCellState(colDiff + position, rowIndex)))
            case _ => {}
          }
        }
      })
    })
  }

  private def paintCell(x: Int, y: Int, cellState: CellState): Unit = {
    val color = cellState match {
      case ch.ben_kelly.CellStates.Empty => Color.Transparent
      case ch.ben_kelly.CellStates.Living => Color.Green
    }
    grid.children(grid.columnConstraints.size() * y + x).asInstanceOf[Rectangle].fill = color
  }
}
