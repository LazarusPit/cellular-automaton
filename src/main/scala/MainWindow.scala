package ch.ben_kelly

import scalafx.Includes._
import scalafx.application.JFXApp3
import scalafx.application.JFXApp3.PrimaryStage
import scalafx.scene.{Parent, Scene}
import scalafxml.core.FXMLLoader

object MainWindow extends JFXApp3 with WindowLoader {
  override def start(): Unit = {
    val loader: FXMLLoader = getLoader("/CellularAutomaton.fxml")
    val root: Parent = getWindow(loader)
    val controller: IController = loader.getController[IController]
    controller.init()
    stage = new PrimaryStage() {
      title = "Cellular Automaton"
      scene = new Scene(root)
    }

  }
}
