package ch.ben_kelly

import MainWindow.stage

import javafx.scene.input.MouseEvent
import scalafx.Includes._
import scalafx.scene.control.ButtonBar.ButtonData
import scalafx.scene.control.{Button, ButtonType, Dialog}
import scalafx.scene.layout.{GridPane, VBox}
import scalafxml.core.macros.sfxml

@sfxml
class MainController(private val window: VBox, private val grid: GridPane, btnSettings: Button) extends IController with WindowLoader {

  case class Result()

  private val settingsRoot: scalafx.scene.Parent = {
    val loader = getLoader("/Settings.fxml")
    getWindow(loader)
  }

  override def init(): Unit = {
    btnSettings.onMouseClicked = e => onOpenSettings(e)
  }

  def onOpenSettings(event: MouseEvent): Unit = {
    val loginButtonType = new ButtonType("Save", ButtonData.OKDone)
    new Dialog[Result]() {
      initOwner(stage)
      title = "Settings"
      dialogPane().buttonTypes = Seq(ButtonType.Cancel, loginButtonType)
      dialogPane().content = settingsRoot
      resultConverter = dialogButton =>
        if (dialogButton == loginButtonType) Result() else null
      showAndWait()
    }
  }

}