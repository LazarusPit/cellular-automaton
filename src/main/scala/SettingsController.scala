package ch.ben_kelly

import scalafx.Includes._
import javafx.fxml.FXML
import scalafx.event.ActionEvent
import scalafx.scene.control.{Button, TextField}
import scalafxml.core.macros.sfxml

@sfxml
class SettingsController(textWidth: TextField, textHeight: TextField, textPopulation: TextField, btnCancel: Button, btnSave: Button) extends IController {

  @FXML def onCancel(actionEvent: ActionEvent): Unit = {
    onQuit()
  }

  @FXML def onSave(actionEvent: ActionEvent): Unit = {

  }

  override def init(): Unit = {

  }
}