package ch.ben_kelly.utils;

import javafx.util.converter.IntegerStringConverter;

class PositiveIntegerStringConverter extends IntegerStringConverter {
    override def fromString(value: String): Integer = {
        val result = super.fromString(value)
        if (result < 0) throw new RuntimeException("Negative number")
        result
    }

    override def toString(value: Integer): String = {
        if (value < 0) return "0"
        super.toString(value)
    }
}
