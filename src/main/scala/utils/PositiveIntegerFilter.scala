package ch.ben_kelly
package utils

import javafx.scene.control.TextFormatter

import java.util.function.UnaryOperator

class PositiveIntegerFilter extends UnaryOperator[TextFormatter.Change] {

  override def apply(change: TextFormatter.Change): TextFormatter.Change = {
    if (change.getControlNewText.matches("[0-9]*")) {
      return change
    }
    null
  }
}
