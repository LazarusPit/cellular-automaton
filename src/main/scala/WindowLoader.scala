package ch.ben_kelly

import scalafx.Includes._
import scalafxml.core.{FXMLLoader, NoDependencyResolver}

import java.io.IOException

trait WindowLoader {
  def getLoader(resourceName: String): FXMLLoader = {
    val resource = getClass.getResource(resourceName)
    if (resource == null) {
      throw new IOException(s"Cannot load resource: $resourceName")
    }
    val loader = new FXMLLoader(resource, NoDependencyResolver)
    loader.load()
    loader
  }
  def getWindow(loader: FXMLLoader): javafx.scene.Parent = {
    loader.getRoot[javafx.scene.Parent]
  }

  def getSfxmlWindow(loader: FXMLLoader): scalafx.scene.Parent = {
    loader.getRoot[scalafx.scene.Parent]
  }
}
