package ch.ben_kelly
package models

import CellStates.CellState

import scalafx.collections.ObservableBuffer


class EnvironmentModel(val width: Int, val height: Int, var initPopulation: Int) {
  import EnvironmentModel.CoordinatesOutOfBoundsException

  private val cells: ObservableBuffer[ObservableBuffer[CellStates.CellState]] = ObservableBuffer.fill(height)(ObservableBuffer.fill(width)(CellStates.Empty))

  def getCellsObservable: ObservableBuffer[ObservableBuffer[CellState]] = {
    cells
  }

  private def checkCoordinates(x: Int, y: Int): Unit = {
    if (x < 0 || y < 0 || x >= width || y >= height) {
      throw new CoordinatesOutOfBoundsException(x, y, width, height)
    }
  }

  def getCellState(x: Int, y: Int): CellStates.CellState = {
    checkCoordinates(x, y)
    cells(y)(x)
  }

  def setCellState(x: Int, y: Int, state: CellStates.CellState): Unit = {
    checkCoordinates(x, y)
    cells(y)(x) = state
  }
}

object EnvironmentModel {
  class CoordinatesOutOfBoundsException(x: Int, y: Int, gridWidth: Int, gridHeight: Int) extends Exception(s"Coordinates $x/$y are out of bounds in the environment of $gridWidth x $gridHeight cells.") {

  }
}
