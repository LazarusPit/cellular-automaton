package ch.ben_kelly

object CellStates extends Enumeration {
  type CellState = Value
  val Empty, Living = Value
}
