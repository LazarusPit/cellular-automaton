package ch.ben_kelly

import javafx.event.ActionEvent
import scalafx.application.Platform

trait IController {
  def onQuit(): Unit = {
    Platform.exit()
  }
  def init(): Unit
}
